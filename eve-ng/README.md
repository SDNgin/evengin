[EVE-Ng.net](https://www.eve-ng.net/index.php) ist the place to go and get yourself a bad ass Emulated Virtual Environment!

The community version is outstanding; however, the professional version ist a MUST if you want to use it's full potential.
Invest the 100€/Year for the professional version. The guys at EVE-Ng.net need and deserve support in their continueing
effort providing us with first-class software :-)

Edy and Ada use it extensively on their journey's to becoming professional internetworkers :-D

Kind Regards, SDNScottie
